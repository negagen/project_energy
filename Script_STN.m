tic, clear, clc , close all

%-------------------------------------------------------------------------------------------------
%-------------------------LECTURA DE DATOS--------------------------------------------------------
%-------------------------------------------------------------------------------------------------
Coordenadas_Subestaciones=xlsread('Datos_STN.xlsx','Coordenadas_Subestaciones');
%-------------------------------------------------------------------------------------------------
Generadores=xlsread('Datos_STN.xlsx','Generaci�n');
%-------------------------------------------------------------------------------------------------
Lineas=xlsread('Datos_STN.xlsx','L�neas');
%-------------------------------------------------------------------------------------------------
Transformadores=xlsread('Datos_STN.xlsx','Transformadores','B4:G22');
%-------------------------------------------------------------------------------------------------
Compensacion=xlsread('Datos_STN.xlsx','Compensaci�n','B2:D16');
%-------------------------------------------------------------------------------------------------
Datos=xlsread('Datos_STN.xlsx','Carga','C2:K122');
%-------------------------------------------------------------------------------------------------
VB=xlsread('Datos_STN.xlsx','Carga','B2:B122');
%-------------------------------------------------------------------------------------------------
V=xlsread('Datos_STN.xlsx','Carga','J2:J122')';
d=xlsread('Datos_STN.xlsx','Carga','K2:K122')';
%-------------------------------------------------------------------------------------------------
%-------------------------FIN LECTURA DE DATOS----------------------------------------------------
%-------------------------------------------------------------------------------------------------
Barras_PQ=transpose(find(~Datos(:,2)));
N=length(Datos(:,1));
SB=100e6;   
%-------------------------------------------------------------------------------------------------
%-----------------------FORMULACI�N YBUS----------------------------------------------------------
%-------------------------------------------------------------------------------------------------
IL= Lineas(:,2); 
FL= Lineas(:,3);
r = Lineas(:,4);  
x = Lineas(:,5);
b = Lineas(:,6);     
z = r + 1i*x;                    
y = 1./z;       
b = 1i*b; 
NL = length(IL); 
Y = zeros(N);  
%-------------------------------------------------------------------------------------------------
%--------------------Inclusi�n de l�neas de transmisi�n-------------------------------------------
%-------------------------------------------------------------------------------------------------
 for paso = 1:NL
     Y(IL(paso),FL(paso)) = Y(IL(paso),FL(paso)) - y(paso);
     Y(FL(paso),IL(paso)) = Y(IL(paso),FL(paso));
 end
 % CICLO DE BARRAS 
 for m = 1:N
     for n = 1:NL
         if IL(n) == m
             Y(m,m) = Y(m,m) + y(n) + b(n)/2;
         elseif FL(n) == m
             Y(m,m) = Y(m,m) + y(n) + b(n)/2;
         end
     end
 end           
%-------------------------------------------------------------------------------------------------
%--------------------Inclusi�n de transformadores-------------------------------------------------
%-------------------------------------------------------------------------------------------------
nt=length(Transformadores(:,1));
barra_i=Transformadores(:,1);
barra_j=Transformadores(:,2);
Xtrf=Transformadores(:,3)*1i;
barra_tap=Transformadores(:,4);
Tap=Transformadores(:,5);
% CICLO DE TRANSFORMADORES
for i = 1:nt
    c=0;
    Yeq=0;    
    Ytrf=zeros(2);
    if barra_tap(i) == barra_i(i)
        c=Tap(i);
        Yeq=(1/Xtrf(i))/(abs(c)^2);
    elseif barra_tap(i) == barra_j(i)
        c=1/Tap(i);
        Yeq=(1/Xtrf(i));
    end
        Ytrf(1,1)=Yeq;  
        Ytrf(1,2)=-Yeq*c;
        Ytrf(2,1)=-Yeq*conj(c);
        Ytrf(2,2)=Yeq*abs(c)^2;
        Y(barra_i(i),barra_i(i))=Y(barra_i(i),barra_i(i))+Ytrf(1,1);
        Y(barra_i(i),barra_j(i))=Y(barra_i(i),barra_j(i))+Ytrf(1,2);
        Y(barra_j(i),barra_i(i))=Y(barra_j(i),barra_i(i))+Ytrf(2,1);
        Y(barra_j(i),barra_j(i))=Y(barra_j(i),barra_j(i))+Ytrf(2,2);
end
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%---------------------------FIN FORMULACI�N YBUS--------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------INICIO NEWTON RAPHSON---------------------------------------------------
%-------------------------------------------------------------------------------------------------
%----------------POTENCIAS PROGRAMADAS------------------------------------------------------------
P_prog=zeros(1,N-1);
for i = 1 : N-1
    P_prog(i)=(Datos(i,5)-Datos(i,3))*(1e6/SB);
end
Q_prog=zeros(1,length(Barras_PQ));
for i = 1 : length(Barras_PQ)
    Q_prog(i)=(Datos(Barras_PQ(i),7)-Datos(Barras_PQ(i),4))*(1e6/SB);
end
%------------------------INICIO DE LAS ITERACIONES------------------------------------------------
for iteracion = 1 : 50
%----------------POTENCIAS CALCULADAS-------------------------------------------------------------
P_calc=zeros(1,N-1);
for i = 1 : N-1
    for j = 1 : N
        P_calc(i)=P_calc(i)+(V(i)*abs(Y(i,j))*V(j)*cos(angle(Y(i,j))+d(j)-d(i)));
    end
end
Q_calc=zeros(1,length(Barras_PQ));
for i = 1 : length(Barras_PQ)
    for j = 1 : N
        Q_calc(i)=Q_calc(i)-(V(Barras_PQ(i))*abs(Y(Barras_PQ(i),j))*V(j)*sin(angle(Y(Barras_PQ(i),j))+d(j)-d(Barras_PQ(i))));
    end
end
%--------------------VECTOR DE FUNCIONES----------------------------------------------------------
DF=transpose([P_prog-P_calc Q_prog-Q_calc]);
%-------------------------------------------------------------------------------------------------
%---------------------JACOBIANO-------------------------------------------------------------------
%----------------Derivadas de P con respecto a deltas---------------------------------------------
pd=zeros(N-1);
for i = 1 : N-1
    for j = 1 : N-1
        if i == j
            for n = 1 : N
                if i == n
                    pd(i,j)=pd(i,j);
                else
                    pd(i,j)=pd(i,j)+(V(i)*abs(Y(i,n))*V(n)*sin(angle(Y(i,n))+d(n)-d(i)));
                end
            end
        else
            pd(i,j)=-(V(i)*abs(Y(i,j))*V(j)*sin(angle(Y(i,j))+d(j)-d(i)));
        end
    end
end
pd;
%----------------Derivadas de P con respecto a voltajes-------------------------------------------
pv=zeros(N-1,length(Barras_PQ));
for i = 1 : N-1
    for j = 1 : length(Barras_PQ)
        k=Barras_PQ(j);
        if i == k
            for n = 1 : N
                pv(i,j)=pv(i,j)+(V(n)*abs(Y(i,n))*cos(angle(Y(i,n))+d(n)-d(i)));
            end
            pv(i,j)=pv(i,j)+(V(i)*abs(Y(i,i))*cos(angle(Y(i,i))));
        else
            pv(i,j)=(V(i)*abs(Y(i,k))*cos(angle(Y(i,k))+d(k)-d(i)));
        end
    end
end
pv;
%----------------Derivadas de Q con respecto a deltas---------------------------------------------
qd=zeros(length(Barras_PQ),N-1);
for i = 1 : length(Barras_PQ)
    for j = 1 : N-1
        m=Barras_PQ(i);
        if m == j+1
            for n = 1 : N
                if m == n
                    qd(i,j)=qd(i,j);
                else
                    qd(i,j)=qd(i,j)+(V(m)*abs(Y(m,n))*V(n)*cos(angle(Y(m,n))+d(n)-d(m)));
                end
            end
        else 
            qd(i,j)=-(V(m)*abs(Y(m,j+1))*V(j+1)*cos(angle(Y(m,j+1))+d(j+1)-d(m)));
        end
    end
end
qd;
%----------------Derivadas de Q con respecto a voltajes-------------------------------------------
qv=zeros(length(Barras_PQ));
for i = 1 : length(Barras_PQ)
    for j = 1 : length(Barras_PQ)
        m=Barras_PQ(i);
        k=Barras_PQ(j);
        if m == k
            for n = 1 : N
                qv(i,j)=qv(i,j)-(V(n)*abs(Y(m,n))*sin(angle(Y(m,n))+d(n)-d(m)));
            end
            qv(i,j)=qv(i,j)-(V(m)*abs(Y(m,m))*sin(angle(Y(m,m))));
        else
            qv(i,j)=-(V(m)*abs(Y(m,k))*sin(angle(Y(m,k))+d(k)-d(m)));
        end
    end
end
qv;
%-------------------------------------------------------------------------------------------------
J=[pd pv ; qd qv];
%-----------------------------C�LCULO DE LAS SENSIBILIDADES---------------------------------------
DX=J\DF;
%-----------------------------CORRECCI�N DE LAS VARIABLES-----------------------------------------
for i = 1 : N-1
    d(i)=d(i)+DX(i);
end
for i = 1 : length(Barras_PQ)
    V(Barras_PQ(i))=V(Barras_PQ(i))+DX(N-1+i);
end
%-------------------------------------------------------------------------------------------------
%----------------------------FIN DE LAS ITERACIONES-----------------------------------------------
%-------------------------------------------------------------------------------------------------
end
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-----------------------------FIN DEL NEWTON RAPHSON----------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
V_Newton=V;
d_Newton=d*180/pi;
%-------------------------------------------------------------------------------------------------
%-------------------POTENCIAS NETAS INYECTADAS A LAS BARRAS---------------------------------------
%-------------------------------------------------------------------------------------------------
Pi=zeros(1,N);
for i=1:N
    for j=1:N
        Pi(i)=Pi(i)+(abs(V(i))*abs(Y(i,j))*abs(V(j))*cos(angle(Y(i,j))+d(j)-d(i))); 
    end    
end
Qi=zeros(1,N);
for i=1:N
    for j=1:N
        Qi(i)=Qi(i)-(abs(V(i))*abs(Y(i,j))*abs(V(j))*sin(angle(Y(i,j))+d(j)-d(i))); 
    end    
end
Pi=Pi*SB;
Qi=Qi*SB;  
for i = 1 : N
    if abs(Pi(i)) < 0.0001
        Pi(i)=0;
    end
    if abs(Qi(i)) < 0.0001
        Qi(i)=0;
    end
end
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%----------------C�ULCULO DE FLUJOS DE POTENCIA EN EL SISTEMA-------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%--------------Flujo en las l�neas----------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%Par�metros
A=zeros(NL,1);
for i = 1 : NL
    A(i)=(1+(((b(i))*(z(i)))/2));
end
B=zeros(NL,1);
for i = 1 : NL
    B(i)=z(i)*(1+(((b(i))*(z(i)))/6));
end
C=zeros(NL,1);
for i = 1 : NL
    C(i)=b(i)*(1+(((b(i))*(z(i)))/6));
end
%Recepci�n
IRL=zeros(NL,1);
for i = 1 : NL
    IRL(i)=((((V(IL(i)))*exp(d(IL(i))*1i))/B(i))-((A(i)*((V(FL(i)))*exp(d(FL(i))*1i)))/B(i)));
end
SRL=zeros(NL,1);
for i = 1 : NL
    SRL(i)=((V(FL(i))*exp(d(FL(i))*1i))*conj(IRL(i)))*SB;
end
%Emisi�n
ISL=zeros(NL,1);
for i = 1 : NL
    ISL(i)=((C(i)*(V(FL(i))*exp(d(FL(i))*1i)))+(A(i)*IRL(i)));
end
SSL=zeros(NL,1);
for i = 1 : NL
    SSL(i)=((V(IL(i))*exp(d(IL(i))*1i))*conj(ISL(i)))*SB;
end
%-------------------------------------------------------------------------------------------------
%--------------------FLUJO EN LOS TRANSFORMADORES--------------------------------------------------
%-------------------------------------------------------------------------------------------------
SStrf=zeros(nt,1);
SRtrf=zeros(nt,1);
for i = 1 : nt
    c=0;
    Yeq=0;    
    Ytrf=zeros(2);
    if barra_tap(i) == barra_i(i)
        c=Tap(i);
        Yeq=(1/Xtrf(i))/(abs(c)^2);
    elseif barra_tap(i) == barra_j(i)
        c=1/Tap(i);
        Yeq=(1/Xtrf(i));
    end
        Ytrf(1,1)=Yeq;  
        Ytrf(1,2)=-Yeq*c;
        Ytrf(2,1)=-Yeq*conj(c);
        Ytrf(2,2)=Yeq*abs(c)^2;
        It=(Ytrf*[V(barra_i(i))*exp(d(barra_i(i))*1i); V(barra_j(i))*exp(d(barra_j(i))*1i)]);
        SStrf(i)=((V(barra_i(i))*exp(d(barra_i(i))*1i))*conj(It(1)))*SB;
        SRtrf(i)=((V(barra_j(i))*exp(d(barra_j(i))*1i))*conj(-It(2)))*SB;
end
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%---------------------------FIN C�CULO DE FLUJOS EN EL SISTEMA------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------


%-------------------------------------------------------------------------------------------------
%----------------------------------RESULTADOS-----------------------------------------------------
%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
xlswrite('Resultados_STN.xlsx',Y,'YBUS');
%-------------------------------------------------------------------------------------------------
V_Real=V_Newton'.*VB;
Resultado_Tensiones=[Datos(:,1) V_Newton' V_Real d_Newton' Pi'/1e6 Qi'/1e6];
xlswrite('Resultados_STN.xlsx',Resultado_Tensiones,'Tensi�n barras','B2:G122');
%-------------------------------------------------------------------------------------------------
Gen_Slack=(Pi(N)+Datos(N,3)*1e6)/1e6;
xlswrite('Datos_STN.xlsx',Gen_Slack,'Generaci�n','C32');
%-------------------------------------------------------------------------------------------------
VB_Lineas=Lineas(:,8);
IB=(SB)./(sqrt(3).*(VB_Lineas*1e3));
IR_Lineas=[abs(IRL).*IB angle(IRL)*180/pi];
IS_Lineas=[abs(ISL).*IB angle(ISL)*180/pi];
Flujo_emision_Lin=[real(SSL) imag(SSL)];
Flujo_recepcion_Lin=[ real(SRL) imag(SRL)];
MVA_emision_Lin=abs(SSL);
MVA_recepcion_Lin=abs(SRL);
Capacidad_Lin=Lineas(:,7);
Cargabilidad_Lin=(max(MVA_emision_Lin,MVA_recepcion_Lin)./(Capacidad_Lin*1e6));
PERDIDAS_LINEAS=[real(SSL)-real(SRL) imag(SSL)-imag(SRL)];
Resultado_Lin=[IL FL IS_Lineas IR_Lineas Flujo_emision_Lin/1e6 Flujo_recepcion_Lin/1e6 PERDIDAS_LINEAS/1e6 max(MVA_emision_Lin,MVA_recepcion_Lin)/1e6 Capacidad_Lin Cargabilidad_Lin];
xlswrite('Resultados_STN.xlsx',Resultado_Lin,'Flujo L�neas','A3:O233');
%-------------------------------------------------------------------------------------------------
Flujo_emision_trf=[ real(SStrf) imag(SStrf)];
Flujo_recepcion_trf=[ real(SRtrf) imag(SRtrf)];
MVA_emision_trf=abs(SStrf);
MVA_recepcion_trf=abs(SRtrf);
Capacidad_trf=Transformadores(:,6);
Cargabilidad_trf=(max(MVA_emision_trf,MVA_recepcion_trf)./(Capacidad_trf*1e6));
PERDIDAS_TRF=Flujo_emision_trf-Flujo_recepcion_trf;
Resultado_trf=[barra_i barra_j Flujo_emision_trf/1e6 Flujo_recepcion_trf/1e6 PERDIDAS_TRF/1e6 max(MVA_emision_trf,MVA_recepcion_trf)/1e6 Capacidad_trf Cargabilidad_trf];
xlswrite('Resultados_STN.xlsx',Resultado_trf,'Flujo Transformadores','A3:K21');
%-------------------------------------------------------------------------------------------------
Flujo_Gen=zeros(length(Generadores(:,1)),1);
for i = 1 : length(Generadores(:,1))
    Flujo_Gen(i)=(Pi(Generadores(i))/1e6+Datos(Generadores(i),3))+(Qi(Generadores(i))/1e6+Datos(Generadores(i),4))*1i;
end
MVA_Gen=abs(Flujo_Gen);
Capacidad_Gen=Generadores(:,5);
Cargabilidad_Gen=MVA_Gen./Capacidad_Gen;
Resultado_Gen=[Generadores(:,1) real(Flujo_Gen) imag(Flujo_Gen) MVA_Gen Capacidad_Gen Cargabilidad_Gen ];
xlswrite('Resultados_STN.xlsx',Resultado_Gen,'Flujo Generadores','A2:F32');
Condicion_MVAR=zeros(length(Generadores(:,1)),1);
for i = 1 : length(Generadores(:,1))
    if imag(Flujo_Gen(i)) > Generadores((i),3) && imag(Flujo_Gen(i)) < Generadores((i),4)
        Condicion_MVAR(i)=0;
    else
        Condicion_MVAR(i)=1;
    end
end    
xlswrite('Resultados_STN.xlsx',Condicion_MVAR,'Flujo Generadores','G2:G32');




%-------------------------------------------------------------------------------------------------
% Aux_1_posiciones=zeros(N,1);
% for i = 1 : length(Barras_PQ)
%     if Datos(Barras_PQ(i),7) ~= 0
%         Aux_1_posiciones(i)=Barras_PQ(i);
%     end
% end
% Aux_2_posiciones=find(Aux_1_posiciones);
% Posiciones_Compensadores_PQ=zeros(length(Aux_2_posiciones),1);
% for i = 1 : length(Aux_2_posiciones)
%     Posiciones_Compensadores_PQ(i)=Aux_1_posiciones(Aux_2_posiciones(i));
% end
% 
% Gen_Compensadores_PQ=zeros(length(Posiciones_Compensadores_PQ),1);
% for i = 1 : length(Posiciones_Compensadores_PQ)
%     Gen_Compensadores_PQ(i)=Datos(Posiciones_Compensadores_PQ(i),7)*1e6;
% end
% %-------------------------------------------------------------------------------------------------
% Posiciones_Compensadores_PV=zeros(N,1);
% for i = 1 : N-3
%     if Datos(i+3,2)==2
%         Posiciones_Compensadores_PV(i+3)=i+3;
%     end
% end
% Posiciones_Compensadores_PV=find(Posiciones_Compensadores_PV);
% Gen_Compensadores_PV=zeros(length(Posiciones_Compensadores_PV),1);
% for i = 1 : length(Posiciones_Compensadores_PV)
%     Gen_Compensadores_PV(i)=Qi(Posiciones_Compensadores_PV(i))+(Datos(Posiciones_Compensadores_PV(i),4))*1e6;
% end
% %-------------------------------------------------------------------------------------------------
% Posiciones_Compensadores=[Posiciones_Compensadores_PV ; Posiciones_Compensadores_PQ];
% Gen_Compensadores=[Gen_Compensadores_PV ; Gen_Compensadores_PQ ];
% Resultados_Compensadores=[Posiciones_Compensadores Gen_Compensadores/1e6];
% xlswrite('Datos_Proyecto_SEP.xlsx',Resultados_Compensadores,'Resultados','P3:Q7');
% %-------------------------------------------------------------------------------------------------
% GENERACION_TOTAL=[sum(Flujo_Gen(:,1)) sum(Flujo_Gen(:,2))+sum(Gen_Compensadores)]/1e6;
% xlswrite('Datos_Proyecto_SEP.xlsx',GENERACION_TOTAL,'Resultados','A19:B19');
% %-------------------------------------------------------------------------------------------------
% %-------------------------------------------------------------------------------------------------
% %-------------------------------------------------------------------------------------------------
% %-------------------------------FIN RESULTATODS---------------------------------------------------
% %-------------------------------------------------------------------------------------------------



%-------------------------------------------------------------------------------------------------
%-------------------------------------------------------------------------------------------------
%--------------------------------LOCALIZACI�N GEOGR�FICA------------------------------------------
Latitud=Coordenadas_Subestaciones(:,3)';
Longitud=Coordenadas_Subestaciones(:,4)';
for i = 1 : N
    if Coordenadas_Subestaciones(i,1) == 500
        geoplot(Latitud(i),Longitud(i),'ro','MarkerSize',8);
        hold on
    else
        geoplot(Latitud(i),Longitud(i),'bo','MarkerSize',5);
        hold on
    end
end
% for i = 1 : NL
%     if Lineas(i,8) == 220
%         geoplot([Coordenadas_Subestaciones(IL(i),3) Coordenadas_Subestaciones(FL(i),3)],[Coordenadas_Subestaciones(IL(i),4) Coordenadas_Subestaciones(FL(i),4) ],'b-');
%     elseif Lineas(i,8) == 500
%         geoplot([Coordenadas_Subestaciones(IL(i),3) Coordenadas_Subestaciones(FL(i),3)],[Coordenadas_Subestaciones(IL(i),4) Coordenadas_Subestaciones(FL(i),4) ],'r-');
%     end
% end

        


Coordenadas_Lineas=xlsread('Datos_STN.xlsx','Coordenadas_Lineas','D2:M232');
for i = 1 : NL
    if Lineas(i,8) == 220
        geoplot([Coordenadas_Lineas(i,1) Coordenadas_Lineas(i,2) Coordenadas_Lineas(i,3) Coordenadas_Lineas(i,4) Coordenadas_Lineas(i,5)],...
            [Coordenadas_Lineas(i,6) Coordenadas_Lineas(i,7) Coordenadas_Lineas(i,8) Coordenadas_Lineas(i,9) Coordenadas_Lineas(i,10)],'b-');
        hold on
    else
        geoplot([Coordenadas_Lineas(i,1) Coordenadas_Lineas(i,2) Coordenadas_Lineas(i,3) Coordenadas_Lineas(i,4) Coordenadas_Lineas(i,5)],...
            [Coordenadas_Lineas(i,6) Coordenadas_Lineas(i,7) Coordenadas_Lineas(i,8) Coordenadas_Lineas(i,9) Coordenadas_Lineas(i,10)],'r-');
        hold on
    end
end
title('STN COLOMBIANO 230 kV - 500 kV')
geolimits([-5 13],[-80 -63])
% geobasemap 'landcover'
%-------------------------------------------------------------------------------------------------
Fin='��� FIN DE LA SIMULACI�N !!!';
fprintf('\n %s \n',Fin)
toc







